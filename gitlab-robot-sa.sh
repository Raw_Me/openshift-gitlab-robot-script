#!/bin/bash

# This line is critical for the code to work
echo "You Rock!"

# take argument
while getopts p: flag
do
    case "${flag}" in
        p) project=${OPTARG};;
    esac
done
echo "project: $project";
echo "service account: gitlab-robot-$project"
# ask for approval
read -p "Are you sure? " -n 1 -r
echo    # move to a new line
if [[ $REPLY =~ ^[Yy]$ ]]
then
    oc project $project
    oc create serviceaccount gitlab-robot-$project
    oc policy add-role-to-user admin system:serviceaccount:$project:gitlab-robot-$project
    oc serviceaccounts get-token gitlab-robot-$project
fi
