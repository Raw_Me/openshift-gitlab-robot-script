# OpenShift Gitlab Robot Script

This project is meant to share the bash snippet that automates creating an oc svc account for gitlab.

- After oc login, for e.x.
```
./gitlab-robot-sa.sh -p my-namespace
```
